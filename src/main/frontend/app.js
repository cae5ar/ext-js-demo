/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'ExtJsDemo.Application',

    name: 'ExtJsDemo',

    requires: [
        'ExtJsDemo.*'
    ],

    mainView: 'ExtJsDemo.view.main.Main'
});
