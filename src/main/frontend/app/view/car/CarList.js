/**
 * This view is an example list of people.
 */
Ext.define('ExtJsDemo.view.car.CarList', {
    extend: 'Ext.grid.Grid',
    requires: [
        'Ext.grid.plugin.PagingToolbar',
    ],
    bind: {
        selection: '{currentCar}',
        store: '{cars}',
    },
    title: 'Cars',
    plugins: [{
        type: 'pagingtoolbar'
    }],

    listeners: {
        beforeselect: 'rejectChanges',
        select: 'onItemSelected'
    },

    columns: [{
        text: 'Name',
        dataIndex: 'name',
        flex: 1,
    }, {
        text: 'Origin',
        dataIndex: 'origin',
        flex: 1,
    }, {
        text: 'Cylinders',
        dataIndex: 'cylinders',
        flex: 1,
    }, {
        text: 'Horse Power',
        dataIndex: 'horsePower',
        flex: 1,
    }, {
      text: 'Weight',
      dataIndex: 'weight',
      flex: 1,
    }, {
        text: 'Acceleration',
        dataIndex: 'acceleration',
        flex: 1,
    }, {
        text: 'year',
        dataIndex: 'year',
        flex: 1,
    }],

    items: [{
        xtype: 'toolbar',
        docked: 'top',
        items: [{
            text: 'New',
            handler: 'newCar'
        }, {
            text: 'Delete',
            handler: 'deleteCar',
            bind: {
                disabled: '{!currentCar}'
            }
        }, '->',{
            xtype: 'component',
            bind: {
                html: 'Number of Cars: {numberOfCars}'
            }
        },'->', {
            label: 'Origin',
            xtype: 'combobox',
            reference: 'originFilter',
            publishes: 'value',
            labelAlign: 'top',
            displayField: 'name',
            valueField: 'value',
            value: '',
            editable: false,
            bind: {
                store: '{originsForFilter}'
            },
        }, {
            label: 'Name',
            labelAlign: 'top',
            xtype: 'textfield',
            listeners: {
                change: {
                    fn: 'onNameFilterChange',
                    buffer: 500
                }
            },
            clearable: true
        }]
    }],


});
