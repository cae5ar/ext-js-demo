Ext.define('ExtJsDemo.view.car.CarEdit', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.field.ComboBox',
        'Ext.field.Text',
        'Ext.field.Number'
    ],

    reference: 'carEdit',
    bodyPadding: 5,
    width: '100%',
    defaultFocus: 'textfield[name=carName]',
    title: 'Edit',
    bind: {
        disabled: '{!currentCar}',
        title: 'Edit: {currentCar.name}'
    },
    defaults: {
        labelAlign: 'left',
    },
    defaultType: 'numberfield',

    modelValidation: true,
    items: [
        {
        xtype: 'textfield',
        label: 'Name',
        name: 'carName',
        bind: '{currentCar.name}'
    }, {
        label: 'Origin',
        xtype: 'combobox',
        name: 'origin',
        displayField: 'name',
        valueField: 'value',
        queryMode: 'local',
        forceSelection: true,
        editable: false,
        bind: {
            store: '{originsForEditing}',
            value: '{currentCar.origin}'
        },
    }, {
        label: 'Horse Power',
        name: 'horsePower',
        bind: '{currentCar.horsePower}',
    }, {
        label: 'Cylinders',
        name: 'cylinders',
        bind: '{currentCar.cylinders}',
    }, {
      label: 'Weight',
      name:  'weight',
      bind: '{currentCar.weight}',
    }, {
        label: 'Acceleration',
        name: 'acceleration',
        bind: '{currentCar.acceleration}',
    }, {
        label: 'year',
        name: 'year',
        bind: '{currentCar.year}',
        maxValue: 2018,
    }],
    buttons: [{
        text: 'Reset',
        iconCls: 'x-fa fa-ban',
        handler: 'onCarEditReset',
        disabled: true,
        bind: {
            disabled: '{!status.dirty}'
        }
    }, {
        text: 'Save',
        iconCls: 'x-fa fa-floppy-o',
        disabled: true,
        bind: {
            disabled: '{!status.dirtyAndValid}'
        },
        handler: 'onCarEditSubmit'
    }]
});
