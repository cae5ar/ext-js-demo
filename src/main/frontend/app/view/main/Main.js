/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 */
Ext.define('ExtJsDemo.view.main.Main', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.layout.HBox',
        'Ext.panel.Panel'
    ],
    controller: 'main',
    viewModel: 'main',
    layout: {
        type: 'hbox'
    },

    items: [
        {
            xclass: 'ExtJsDemo.view.car.CarList',
            flex: 1
        },
        {
            xtype:'panel',
            items:[
                {
                    xclass: 'ExtJsDemo.view.car.CarEdit',
                    width: 500
                }
            ]
        }

    ]
});
