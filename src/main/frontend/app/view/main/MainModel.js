/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('ExtJsDemo.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    requires: [
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json'
    ],

    data: {
        currentCar: null,
        numberOfCars: 0,
        nameFilter: null,
    },

    stores: {
        cars: {
            model: 'ExtJsDemo.model.Car',
            pageSize: 50,
            autoLoad: true,
            remoteSort: true,
            remoteFilter: true,
            autoSync: false,
            listeners: {
                load: 'onCarsLoad'
            },
            filters: [{
                property: 'origin',
                value: '{originFilter.value}'
            }, {
                property: 'name',
                value: '{nameFilter}'
            }]
        },
        originsForFilter: {
            fields: ['value', 'name'],
            autoLoad: true,
            data: [{
                name: 'All',
                value: '',
            }],
            listeners: {
                load: function (store) {
                    store.insert(0, [{
                        name: 'All',
                        value: ''
                    }]);
                }
            },

            proxy: {
                type: 'rest',
                url: `${serverUrl}api/origins`,
                reader: {
                    type: 'json',
                }
            }
        },
        originsForEditing: {
            source: '{originsForFilter}',
            filters: [{
                property: 'value',
                value: '',
                operator: '!='
            }]
        }
    },

    formulas: {
        status: {
            bind: {
                bindTo: '{currentCar}',
                deep: true
            },
            get: (car) => {
                const ret = {
                    dirty: car ? car.dirty : false,
                    valid: car && car.isModel ? car.isValid() : false
                };
                ret.dirtyAndValid = ret.dirty && ret.valid;
                return ret;
            }
        }
    }

});
