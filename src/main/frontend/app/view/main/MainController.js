/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('ExtJsDemo.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main',

    onCarsLoad: function (result) {
        const total = result.getTotalCount();
        this.getViewModel().set('numberOfCars', total);

        if (total === 1) {
            this.getViewModel().set('currentCar', s.first());
        }
        else {
            this.getViewModel().set('currentCar', null);
        }
    },

    rejectChanges: function (sender, selectedRecords) {
        if (this.selectedRecord && this.selectedRecord.dirty) {
            this.selectedRecord.reject();
        }
    },

    onItemSelected: function (sender, selectedRecords) {
        this.selectedRecord = selectedRecords[0];
    },

    onNameFilterChange: function (field, newValue) {
        this.getViewModel().set('nameFilter', newValue);
    },

    newCar: function () {
        const newCar = new ExtJsDemo.model.Car();
        this.getViewModel().set('currentCar', newCar);
        Ext.defer(() => {
            this.lookup('carEdit').focus();
            this.lookup('carEdit').isValid();
        }, 5);
    },

    deleteCar: function () {
        const car = this.getViewModel().get('currentCar');
        Ext.Msg.confirm('Confirm', 'Are you sure you want to<br>delete car <b>' + car.get('name') + '</b>?', this.onDeleteConfirm, this);
    },

    onDeleteConfirm: function (choice) {
        if (choice === 'yes') {
            const currentCar = this.getViewModel().get('currentCar');
            currentCar.erase();
            this.getStore('cars').reload();
        }
    },

    onCarEditReset: function () {
        this.getCurrentCar().reject();
    },

    onCarEditSubmit: function () {
        const car = this.getCurrentCar();
        car.save({
            success: () => this.getStore('cars').reload(),
            failure: (record, op) => {
                Ext.Msg.alert('Егrог');
            }
        });
    },
    getCurrentCar: function () {
        return this.getViewModel().get('currentCar');
    }
});
