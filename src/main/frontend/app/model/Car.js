Ext.define('ExtJsDemo.model.Car', {
    extend: 'Ext.data.Model',
    identifier: {
        type: 'negative'
    },
    requires: [
        "Ext.data.identifier.Negative",
        "Ext.data.validator.Length",
        "Ext.data.validator.Presence",
        'Ext.data.validator.Range',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json'
    ],
    fields: [
        {
            name: 'cylinders',
            type: 'integer',
            validators: [{
                type: "presence"
            }, {
                type: "range",
                min: 1,
                max: 99,
            }]
        },
        {
            name: 'horsePower',
            type: 'integer',
            allowNull: true,
        },
        {
            name: 'weight',
            type: 'integer',
            validators: [{
                type: "presence"
            }, {
                type: "range",
                min: 0.001,
                max: 9999,
            }]
        },
        {
            name: 'acceleration',
            type: 'number',
            validators: [{
                type: "presence"
            }, {
                type: "range",
                min: 0.001,
                max: 9999,
            }]
        },
        {
            name: 'year',
            type: 'integer',
            validators: [{
                type: "presence"
            }, {
                type: "range",
                min: 0,
                max: 2018,
            }]
        },
        {
            name: 'origin',
            type: "string",
            validators: [{
                type: "length",
                min: 1,
                max: 255
            }, {
                type: "presence"
            }]
        },
        {
            name: 'name',
            type: "string",
            validators: [{
                type: "length",
                min: 1,
                max: 255
            }, {
                type: "presence"
            }]
        }
    ],
    proxy: {
        type: 'rest',
        url: `${serverUrl}api/cars`,
        reader: {
            type: 'json',
            rootProperty: 'content',
            totalProperty: 'totalElements'
        },
        writer: {
            writeAllFields: true
        }
    }
});
