package com.example.demo.web.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ExtPageRequest implements Serializable {

    private Integer page;
    private Integer start;
    private Integer limit;
    private List<ExtSort> sort = new ArrayList<>();
    private List<ExtFilter> filter = new ArrayList<>();

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public List<ExtSort> getSort() {
        if (sort == null)
            sort = new ArrayList<>();
        return sort;
    }

    public void setSort(String json) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            ExtSort[] values = mapper.readValue(json, ExtSort[].class);
            this.sort.addAll(Arrays.asList(values));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<ExtFilter> getFilters() {
        return filter;
    }

    public void setFilter(String json) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            ExtFilter[] values = mapper.readValue(json, ExtFilter[].class);
            this.filter.addAll(Arrays.asList(values));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Pageable toPageable() {
        List<Order> orders = this.getSort().stream().map(ExtSort::toSpringDataSort).collect(Collectors.toList());
        // Ext JS pages starts with 1, Spring Data starts with 0
        int page = Math.max(this.getPage() - 1, 0);
        if (orders.isEmpty())
            return new PageRequest(page, this.getLimit());
        else
            return new PageRequest(page, this.getLimit(), new Sort(orders));
    }

}
