package com.example.demo.web.dto;

import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

public class ExtSort {

    public enum SortDirection {
        ASC("ASC"), DESC("DESC");

        public String name;

        SortDirection(String name) {
            this.name = name;
        }
    }

    private String property;
    private SortDirection direction;

    public ExtSort() {
    }

    public ExtSort(String property, SortDirection direction) {
        this.property = property;
        this.direction = direction;
    }

    public SortDirection getDirection() {
        return direction;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public void setDirection(SortDirection direction) {
        this.direction = direction;

    }

    public Order toSpringDataSort() {
        return new Order(this.direction == SortDirection.ASC ? Direction.ASC : Direction.DESC, this.property);
    }

}