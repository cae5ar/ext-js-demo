package com.example.demo.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class ExtFilter {

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ExtFilterValue {

        protected String value;

        public ExtFilterValue() {
        }

        public ExtFilterValue(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }

    protected String property;
    protected ExtFilterValue value;

    public ExtFilter() {
    }

    public ExtFilter(String property, ExtFilterValue value) {
        this.property = property;
        this.value = value;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getValue() {
        return value == null ? null : value.getValue();
    }

    public void setValue(ExtFilterValue value) {
        this.value = value;
    }
}