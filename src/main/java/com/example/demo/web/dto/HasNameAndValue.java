package com.example.demo.web.dto;

public interface HasNameAndValue {

    String getName();

    String getValue();
}
