package com.example.demo.web;

import com.example.demo.domain.Car;
import com.example.demo.domain.OriginPlace;
import com.example.demo.repository.CarRepository;
import com.example.demo.specificaiton.CarSpecification;
import com.example.demo.web.dto.ExtPageRequest;
import com.example.demo.web.dto.HasNameAndValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CarResource {

    @Autowired
    private CarRepository carRepository;

    @GetMapping(value = "origins")
    public ResponseEntity<List<HasNameAndValue>> getOrigins() {
        List originPlaces = Arrays.asList(OriginPlace.values());
        return new ResponseEntity<>(originPlaces, HttpStatus.OK);
    }

    @GetMapping(value = "cars")
    public ResponseEntity<Page<Car>> get(@ModelAttribute ExtPageRequest extPageRequest) {
        CarSpecification carSpecification = new CarSpecification(extPageRequest.getFilters());
        Page<Car> page = carRepository.findAll(carSpecification, extPageRequest.toPageable());
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @PostMapping(value = "cars")
    public ResponseEntity<Car> create(@RequestBody Car car) {
        if (car == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        // ID might be exists,
        car.eraseId();
        car = carRepository.saveAndFlush(car);
        return new ResponseEntity<>(car, HttpStatus.CREATED);
    }

    @GetMapping(value = "cars/{id}")
    public ResponseEntity<Car> get(@PathVariable Long id) {
        Car car = carRepository.findOne(id);
        return new ResponseEntity<>(car, HttpStatus.FOUND);
    }

    @PutMapping(value = "cars/{id}")
    public ResponseEntity<Car> update(@PathVariable Long id, @RequestBody Car data) {
        if (data == null || id == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        Car car = carRepository.findOne(id);
        if (car == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        car.copyData(data);
        car = carRepository.saveAndFlush(car);
        return new ResponseEntity<>(car, HttpStatus.OK);
    }

    @DeleteMapping(value = "cars/{id}")
    public ResponseEntity<Page<Car>> delete(@PathVariable Long id) {
        if (id == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        carRepository.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
