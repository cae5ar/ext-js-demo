package com.example.demo.domain;

import com.example.demo.web.dto.HasNameAndValue;
import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum OriginPlace implements HasNameAndValue {
    American("American"), European("European"), Japanese("Japanese");

    String displayName;

    OriginPlace(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String getName() {
        return this.displayName;
    }

    @Override
    public String getValue() {
        return this.name();
    }
}
