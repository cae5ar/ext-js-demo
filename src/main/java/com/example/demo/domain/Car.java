package com.example.demo.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "t_cars")
public class Car extends AbstractPersistable<Long> {

    protected Integer cylinders;
    protected Integer horsePower;
    protected Integer weight;
    protected Float acceleration;
    protected Integer year;
    protected String origin;
    protected String name;

    public Integer getCylinders() {
        return cylinders;
    }

    public void setCylinders(Integer cylinders) {
        this.cylinders = cylinders;
    }

    public Integer getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(Integer horsePower) {
        this.horsePower = horsePower;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Float getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(Float acceleration) {
        this.acceleration = acceleration;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void copyData(Car from) {
        this.cylinders = from.cylinders;
        this.horsePower = from.horsePower;
        this.weight = from.weight;
        this.acceleration = from.acceleration;
        this.year = from.year;
        this.origin = from.origin;
        this.name = from.name;
    }

    public void eraseId() {
        super.setId(null);
    }

    @Override
    public String toString() {
        return "Car{" + "cylinders=" + cylinders + ", horsePower=" + horsePower + ", weight=" + weight
                + ", acceleration=" + acceleration + ", year=" + year + ", origin=" + origin + ", name='" + name + '\''
                + '}';
    }
}
