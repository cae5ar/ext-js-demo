package com.example.demo.specificaiton;

import com.example.demo.domain.Car;
import com.example.demo.web.dto.ExtFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CarSpecification implements Specification<Car> {

    public static final String ORIGIN_PROPERTY = "origin";
    public static final String NAME_PROPERTY = "name";
    private List<ExtFilter> filterList;

    public CarSpecification(List<ExtFilter> filterList) {
        this.filterList = filterList;
    }

    @Override
    public Predicate toPredicate(Root<Car> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {
        if (Objects.isNull(filterList) || filterList.isEmpty())
            return null;
        List<Predicate> predicates = filterList.stream().filter(ef -> Objects.nonNull(ef.getValue())
                && !ef.getValue().isEmpty()).map(extFilter -> {
            if (ORIGIN_PROPERTY.equals(extFilter.getProperty())) {
                return cb.and(cb.equal(root.get(ORIGIN_PROPERTY), extFilter.getValue()));
            }
            else if (NAME_PROPERTY.equals(extFilter.getProperty())) {
                String expr = extFilter.getValue().toLowerCase() + "%";
                return cb.and(cb.like(cb.lower(root.get(NAME_PROPERTY)), expr));
            } return null;
        }).filter(Objects::nonNull).collect(Collectors.toList()); if (predicates.isEmpty())
            return null;
        else
            return cb.and(predicates.toArray(new Predicate[1]));
    }
}
